"use client";

import * as React from "react";

export const Button = () => {
  return (
    <button
      className="bg-lime-200 text-purple-500"
      onClick={() => alert("boop")}
    >
      Boop
    </button>
  );
};
